__author__ = 'romeo.dl'

import ckan.plugins.toolkit as toolkit
from pylons import  session
import ckan.model as model

# APIS being called to handle UI
# ACTIONS : api exposed to be called by the CMS side goes here
# API being called by CMS to set the session for color in ckan
@toolkit.side_effect_free
def change_color(context,data_dict=None):
    if (data_dict['color'] == 'default'):
        if 'color' in session:
            del session['color']
            session.save()
    else:
        session['color'] = data_dict['color']
        session.save()

# API being called by CMS to set the session for size in ckan
@toolkit.side_effect_free
def change_size(context,data_dict=None):
    if (data_dict['size'] == 'default'):
        if 'size' in session:
            del session['size']
            session.save()
    else:
        session['size'] = int(data_dict['size'])
        session.save()

# API being called by CMS to set the session for font in ckan
@toolkit.side_effect_free
def change_font(context,data_dict=None):
    if (data_dict['font'] == 'default'):
        if 'font' in session:
            del session['font']
            session.save()
    else:
        session['font'] = data_dict['font']
        session.save()

# API being called by CMS to set the session for theme in ckan
@toolkit.side_effect_free
def change_background(context,data_dict=None):
    if (data_dict['background'] == 'default'):
        if 'background' in session:
            del session['background']
            session.save()
    else:
        session['background'] = data_dict['background']
        session.save()


